package converter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Мейн-Класс, реализующий запуск конвертера(главной страницы)
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../mainPage/converterMainPage.fxml"));
        primaryStage.getIcons().add(new Image("converter\\assets\\icon\\icon.png"));
        primaryStage.setTitle("Конвертер");
        primaryStage.setScene(new Scene(root, 495, 580));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
