package weightPage;

import converter.animation.Shake;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mainPage.ControllerMainPage;

import java.io.IOException;

/**
 * Конвертер массы
 *
 * @author Nikita Lvov
 */
public class ControllerWeight {

    @FXML
    private ImageView imageButtonBack;

    @FXML
    private TextField initialValue;

    @FXML
    private TextField convertedValue;

    @FXML
    private ComboBox<String> comboBox1;

    @FXML
    private ComboBox<String> comboBox2;

    /**
     * Парсинг вещественного значения
     *
     * @return вещественное число
     */
    @FXML
    double input() {
        return Double.parseDouble(initialValue.getText());
    }

    /**
     * Метод отвечающий за очитску полей
     */
    @FXML
    void clear() {
        initialValue.clear();
        convertedValue.clear();
    }

    /**
     * Метод выполняющий конвертацию массы
     */
    @FXML
    void convert() {
        if (initialValue.getText().isEmpty()) {
            Shake valueShake = new Shake(initialValue);
            valueShake.playAnimation();
            initialValue.setStyle("-fx-border-color: red; -fx-background-color: #DCDCDC; -fx-border-radius: 50; -fx-background-radius: 25;");
            return;
        }

        initialValue.setStyle("-fx-background-color: #DCDCDC; -fx-border-radius: 50; -fx-background-radius: 25;");
        String box1 = comboBox1.getValue();
        String box2 = comboBox2.getValue();
//------------------------------------------------------------------------------
        if (box1.equals("Тонна") && box2.equals("Килограмм")) {
            double t = input();

            double kg = t * 1000;

            convertedValue.setText(String.valueOf(kg));
        } else if (box1.equals("Тонна") && box2.equals("Грамм")) {
            double t = input();

            double kg = t * 1e+6;

            convertedValue.setText(String.valueOf(kg));
        } else if (box1.equals("Тонна") && box2.equals("Тонна")) {
            double t = input();

            convertedValue.setText(String.valueOf(t));
        }
//------------------------------------------------------------------------------
        if (box1.equals("Килограмм") && box2.equals("Тонна")) {
            double kg = input();

            double t = kg / 1000;

            convertedValue.setText(String.valueOf(t));
        } else if (box1.equals("Килограмм") && box2.equals("Грамм")) {
            double kg = input();

            double gr = kg * 1000;

            convertedValue.setText(String.valueOf(gr));
        } else if (box1.equals("Килограмм") && box2.equals("Килограмм")) {
            double kg = input();

            convertedValue.setText(String.valueOf(kg));
        }

//------------------------------------------------------------------------------
        if (box1.equals("Грамм") && box2.equals("Тонна")) {
            double gr = input();

            double t = gr / 1e+6;

            convertedValue.setText(String.valueOf(t));
        } else if (box1.equals("Грамм") && box2.equals("Килограмм")) {
            double gr = input();

            double kg = gr / 1000;

            convertedValue.setText(String.valueOf(kg));
        } else if (box1.equals("Грамм") && box2.equals("Грамм")) {
            double gr = input();

            convertedValue.setText(String.valueOf(gr));
        }
    }

    /**
     * Метод, выполняющий переход назад на главную страницу при нажатии на картинку
     */
    @FXML
    void goBack() {
        imageButtonBack.setOnMouseMoved(event -> {
            imageButtonBack.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ControllerMainPage.class.getResource("../mainPage/converterMainPage.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            stage.setTitle("Конвертер");
            stage.getIcons().add(new Image("converter\\assets\\icon\\icon.png"));
            stage.show();
        });
    }
}
