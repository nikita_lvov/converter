package currencyPage;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mainPage.ControllerMainPage;

import java.io.IOException;

public class ControllerCurrency {

    @FXML
    private ImageView imageButtonBack;

    @FXML
    private TextField initialValue;

    @FXML
    private TextField convertedValue;

    @FXML
    private ComboBox<String> comboBox1;

    @FXML
    private ComboBox<String> comboBox2;

    @FXML
    double input() {
        return Double.parseDouble(initialValue.getText());
    }

    @FXML
    void clear() {
        initialValue.clear();
        convertedValue.clear();
    }

    @FXML
    void convert() {

    }

    @FXML
    void goBack() {
        imageButtonBack.setOnMouseMoved(event -> {
            imageButtonBack.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ControllerMainPage.class.getResource("../mainPage/converterMainPage.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            stage.setTitle("Конвертер");
            stage.getIcons().add(new Image("converter\\assets\\icon\\icon.png"));
            stage.show();
        });
    }
}