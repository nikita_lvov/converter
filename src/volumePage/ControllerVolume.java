package volumePage;


import converter.animation.Shake;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mainPage.ControllerMainPage;

import java.io.IOException;

/**
 * Конвертер объема
 *
 * @author Nikita Lvov
 */
public class ControllerVolume {

    @FXML
    private ImageView imageButtonBack;

    @FXML
    private TextField initialValue;

    @FXML
    private TextField convertedValue;

    @FXML
    private ComboBox<String> comboBox1;

    @FXML
    private ComboBox<String> comboBox2;

    /**
     * Парсинг вещественного значения
     *
     * @return вещественное число
     */
    @FXML
    double input() {
        return Double.parseDouble(initialValue.getText());
    }

    /**
     * Метод отвечающий за очитску полей
     */
    @FXML
    void clear() {
        initialValue.clear();
        convertedValue.clear();
    }

    /**
     * Метод выполнющий конвертацию массы
     */
    @FXML
    void convert() {
        if (initialValue.getText().isEmpty()) {
            Shake valueShake = new Shake(initialValue);
            valueShake.playAnimation();
            initialValue.setStyle("-fx-border-color: red; -fx-background-color: #DCDCDC; -fx-border-radius: 50; -fx-background-radius: 25;");
            return;
        }

        initialValue.setStyle("-fx-background-color: #DCDCDC; -fx-border-radius: 50; -fx-background-radius: 25;");
        String box1 = comboBox1.getValue();
        String box2 = comboBox2.getValue();
//------------------------------------------------------------------------------
        if (box1.equals("Миллилитр") && box2.equals("Литр")) {
            double ml = input();

            double l = ml / 1000;

            convertedValue.setText(String.valueOf(l));
        } else if (box1.equals("Миллилитр") && box2.equals("Кубический метр")) {
            double ml = input();

            double km = ml / 1e+6;

            convertedValue.setText(String.valueOf(km));
        } else if (box1.equals("Миллилитр") && box2.equals("Миллилитр")) {
            double ml = input();

            convertedValue.setText(String.valueOf(ml));
        }
//------------------------------------------------------------------------------
        if (box1.equals("Литр") && box2.equals("Миллилитр")) {
            double l = input();

            double ml = l * 1000;

            convertedValue.setText(String.valueOf(ml));
        } else if (box1.equals("Литр") && box2.equals("Кубический метр")) {
            double l = input();

            double km = l / 1000;

            convertedValue.setText(String.valueOf(km));
        } else if (box1.equals("Килограмм") && box2.equals("Литр")) {
            double l = input();

            convertedValue.setText(String.valueOf(l));
        }

//------------------------------------------------------------------------------
        if (box1.equals("Кубический метр") && box2.equals("Миллилитр")) {
            double km = input();

            double ml = km * 1e+6;

            convertedValue.setText(String.valueOf(ml));
        } else if (box1.equals("Кубический метр") && box2.equals("Литр")) {
            double km = input();

            double l = km * 1000;

            convertedValue.setText(String.valueOf(l));
        } else if (box1.equals("Кубический метр") && box2.equals("Кубический метр")) {
            double km = input();

            convertedValue.setText(String.valueOf(km));
        }
    }

    /**
     * Метод, выполняющий переход назад на главную страницу при нажатии на картинку
     */
    @FXML
    void goBack() {
        imageButtonBack.setOnMouseMoved(event -> {
            imageButtonBack.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ControllerMainPage.class.getResource("../mainPage/converterMainPage.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            stage.setTitle("Конвертер");
            stage.getIcons().add(new Image("converter\\assets\\icon\\icon.png"));
            stage.show();
        });
    }
}
