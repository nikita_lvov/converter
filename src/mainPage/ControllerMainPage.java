package mainPage;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * Класс главной страницы с которой осуществляется переход на разные типы конвертеров
 *
 * @author Nikita Lvov
 */
public class ControllerMainPage {

    @FXML
    private ImageView imageButtonWeight;

    @FXML
    private ImageView imageButtonCurrency;

    @FXML
    private ImageView imageButtonVolume;

    @FXML
    private ImageView imageButtonSpeed;

    /**
     * Вкладка конвертера объема
     */
    @FXML
    void volumePage() {
        createNewPage(imageButtonVolume, "../volumePage/converterVolume.fxml", "converter\\assets\\panelMainPage\\volume.png", "Конвертер объема");
    }

    /**
     * Вкладка конвертера скорости
     */
    @FXML
    void speedPage() {
        createNewPage(imageButtonSpeed, "../speedPage/converterSpeed.fxml", "converter\\assets\\panelMainPage\\speed.png", "Конвертер скорости");
    }

    /**
     * Вкладка конвертера валют
     */
    @FXML
    void currencyPage() {
        createNewPage(imageButtonCurrency, "../currencyPage/converterCurrency.fxml", "converter\\assets\\panelMainPage\\currency.png", "Конвертер валют");
    }

    /**
     * Вкладка конвертера массы
     */
    @FXML
    void weightPage() {
        createNewPage(imageButtonWeight, "../weightPage/converterWeight.fxml", "converter\\assets\\panelMainPage\\weight.png", "Конвертер массы");
    }

    /**
     * Метод, реализующий создание новых вкладок при нажатии на картинку
     *
     * @param imageButton панели конвертеров
     * @param path        путь к файлу с дизайном (fxml-файл)
     * @param url         ссылка на фотографию
     * @param title       название вкладки
     */
    private static void createNewPage(ImageView imageButton, String path, String url, String title) {
        imageButton.setOnMouseMoved(event -> {
            imageButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ControllerMainPage.class.getResource(path));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle(title);
            stage.getIcons().add(new Image(url));
            stage.showAndWait();
        });
    }
}
